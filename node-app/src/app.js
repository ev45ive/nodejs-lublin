const
  express = require('express'),
  logger = require('morgan'),
  bodyParser = require('body-parser'),
  session = require('express-session'),
  flash = require('express-flash'),
  usersRoutes = require('./users/routes.js'),
  mongoose = require('mongoose');
app = express();

app.use(session({secret:'alamakotasecret'}))
app.use(flash())

mongoose.connect('mongodb://localhost/test');

// app.set('view engine', 'pug')
app.set('view engine', 'hbs')
app.set('views', './src/views/')

app.use(logger('dev'))
app.use('/static', express.static('./public'))
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/users', usersRoutes)

app.get('/', (req, res) => {
  res.render('index', {
    title: 'Hello PUG!'
  })
})

app.use((err, req, res, next) => {
  res.status(err.status || 500)
  res.render('index',{
    error:err
  })
})

// export default app //es6
module.exports = app