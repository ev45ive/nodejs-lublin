const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: String,
  email: String,
  password: String,
  // pokemons: [PokemonSchema]
})

// UserSchema.method('getPokemons',function(){
//   return this.model('Animal')
//              .find({ type: this.type }, cb);
// })

const User = mongoose.model('User',UserSchema,'users')

module.exports = User