var express = require('express')
var router = express.Router()

router.get('/', (req, res) => {
  res.render('users/list', {
    title: 'User',
    user: {
      name: 'Alice'
    }
  })
})

router.get('/register', (req, res) => {
  res.render('users/register', {
    title: 'Register'
  })
})
router.post('/register', (req, res) => {
  const userData = req.body;
  const User = require('../models/user.js')
  const user = new User(userData)

  user.save()
  .then(() => {
    req.flash('info', 'User Created');
    res.redirect('/users/')
  },(error) => {
    res.render('users/register', {
      error: error,
      title: 'Register',
      user: userData
    })
  })

})

router.get('/:id', (req, res) => {
  res.send('Hello user ' + req.params['id'])
})


module.exports = router