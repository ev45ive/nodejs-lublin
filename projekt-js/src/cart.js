export class Cart {
  constructor(productsIndex, items = []) {
    this._items = items
    this._total = 0
    this.productsIndex = productsIndex
  }

  getItems(){
    return this._items
  }

  updateCart() {
    this._total = this._items.reduce((total, item) => {
      const product = this.productsIndex[item.productId]
      item.subtotal = product.price * item.amount
      total += item.subtotal

      return total
    }, 0)
  }

  addToCart(product) {

    // find product in cart
    const item = this._items.find(item => item.productId == product.id)

    if (item) {
      // increment amount by 1
      item.amount += 1
    } else {
      // add cart item - amount - 1
      this._items.push({
        productId: product.id,
        amount: 1
      })
    }
    this.updateCart()
  }
}