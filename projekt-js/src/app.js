
import { Cart } from './cart.js'
import { CartView, CartConfig } from './cart_view.js'

const products = [
  {
    id: 2,
    name: 'NodeJS',
    price: 200,
    promo: false,
    stock: 0
  },
  {
    id: 1,
    name: 'JavaScript',
    price: 100,
    promo: true,
    stock: 2,
  }
]
const productsIndex = products.reduce((acc, product) => {
  acc[product.id] = product
  return acc
}, {})


const productsElem = document.querySelector('.products')

products.forEach(product => {
  const optionElem = document.createElement('option')
  optionElem.innerText = product.name + ' - ' + product.price
  optionElem.value = product.id
  productsElem.appendChild(optionElem)
})

const cartItems = [
  {
    productId: 1,
    amount: 2
  },
  {
    productId: 2,
    amount: 1
  }
]


const cart = new Cart(productsIndex, cartItems)
cart.updateCart()

const cartView = new CartView(
  document.querySelector('.cart_view'),
  productsIndex,
  cart
)
cartView.render()


const add_to_cart = document.querySelector('.add_to_cart')

add_to_cart.addEventListener('click', () => {
  const id = productsElem.value
  const product = productsIndex[id]

  cart.addToCart(product)
  cartView.render()
})

console.log('hello webpack')