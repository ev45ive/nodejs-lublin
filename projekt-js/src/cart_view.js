import { Cart } from './cart.js'

export class CartView {

  constructor(elem, productsIndex, cart) {
    this._elem = elem
    this.productsIndex = productsIndex

    if (!cart instanceof Cart) { throw TypeError('Instance of Cart needed') }
    this._cart = cart
  }

  render() {
    const itemsElem = this._elem.querySelector('.cart_items')
    itemsElem.textContent = ''

    this._cart.getItems().forEach(item => {

      const itemElem = document.createElement('li')
      itemElem.classList.add('list-group-item')

      const product = this.productsIndex[item.productId]

      itemElem.innerHTML = `
      <b>${product.name}</b> - ${item.amount} x ${product.price.toFixed(2)} 
      = ${item.subtotal.toFixed(2)}
      ${product.promo ? ' PROMOTION!' : ''}`

      itemsElem.appendChild(itemElem)

    })

    const totalElem = this._elem.querySelector('.total')
    totalElem.textContent = this._cart._total.toFixed(2)
  }
}

export const CartConfig = {}