var path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry:[
    './src/app.js'
  ],
  output:{
    filename: 'bundle.js',
    path: path.resolve('./dist')
  },
  plugins: [
    new HtmlWebpackPlugin({template:'./index.html'})
  ],
  mode: 'development',
  module:{
    rules:[
      // {
      //   test: /\.js$/,
      //   use: {
      //     loader:'babel-loader',
      //     options:{
      //       presets:['env']
      //     }
      //   }
      // }
    ]
  }
}